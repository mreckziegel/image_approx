'use strict';

(function(global) {

	const vs = loadSync('./shader/circle_vert.glsl');
	const fs = loadSync('./shader/circle_frag.glsl');

	class CircleEntity extends Entity {
		constructor(origCanvas, renderCanvas, length) {
			super(origCanvas, renderCanvas, vs, fs);
			this.vaoInfo = this.renderer.createVAOInfo(this.renderer.entityProgram, {
				 a_position: { data: new Float32Array(length * 6 * 2), numComponents: 2},
				 a_param: { data: new Float32Array(length * 6 * 3), numComponents: 3},
				 a_color: { data: new Float32Array(length * 6 * 4), numComponents: 4},
			}, length * 6);
			this.sW = Math.min(1, this.height / this.width);
			this.sH = Math.min(1, this.width / this.height);
		}

		seed(userData) {
			var e = [];

			for(var i=0;i<userData.l;i++) {
				var t = {};
				var x = random(-1, 1);
				var y = random(-1, 1);
				t.x = x;
				t.y = y;
				t.rad = 0;
				t.r = random(-1,1);
				t.g = random(-1,1);
				t.b = random(-1,1);
				t.a = 0;
				e.push(t);
			}
			return e;
		}

		mutate(e, userData) {
			var res = [];
			var temp = userData.pt;
	    var cTemp = userData.ct;
			var numMutate = userData.nm;

			for(var i=0;i<e.length;i++) {
				var t = e[i];
				var newT = { x : t.x, y: t.y, rad: t.rad, r: t.r, g: t.g, b: t.b, a: t.a};
				res.push(newT);
	    }

			var idx = [];
			for(var i=0;i<numMutate;i++) {
				idx.push(Math.round(random(0,e.length - 1)));
			}

			for(var i=0;i<numMutate;i++) {
				var t = res[idx[i]];
				if(Math.random() < 0.5) {
					t.x = mutateND(t.x, temp, -1, 1);
					t.y = mutateND(t.y, temp, -1, 1);
					t.rad = mutateND(t.rad, temp, 0, 0.5);
				} else {
					t.r = mutateND(t.r, cTemp, -1, 1);
					t.g = mutateND(t.g, cTemp, -1, 1);
					t.b = mutateND(t.b, cTemp, -1, 1);
					t.a = mutateND(t.a, cTemp, 0, 0.5);
				}
	    }
			return res;
		}

		getUniforms() {
			return {
				sW:this.sW,
				sH:this.sH
			}
		}

		getUpdatedVAOInfo(e) {
			var positions = this.vaoInfo.buffers.a_position.data;
			var params = this.vaoInfo.buffers.a_param.data;
			var colors = this.vaoInfo.buffers.a_color.data;
			var sW = this.sW;
			var sH = this.sH;

			for(var i=0;i<e.length;i++) {
				var t = e[i];
				positions[i * 12] = t.x - t.rad * sW;
				positions[i * 12 + 1] = t.y - t.rad * sH;
				positions[i * 12 + 2] = t.x + t.rad * sW;
				positions[i * 12 + 3] = t.y - t.rad * sH;
				positions[i * 12 + 4] = t.x - t.rad * sW;
				positions[i * 12 + 5] = t.y + t.rad * sH;
				positions[i * 12 + 6] = t.x + t.rad * sW;
				positions[i * 12 + 7] = t.y - t.rad * sH;
				positions[i * 12 + 8] = t.x - t.rad * sW;
				positions[i * 12 + 9] = t.y + t.rad * sH;
				positions[i * 12 + 10] = t.x + t.rad * sW;
				positions[i * 12 + 11] = t.y + t.rad * sH;

				colors[i * 24] = t.r;
				colors[i * 24 + 1] = t.g;
				colors[i * 24 + 2] = t.b;
				colors[i * 24 + 3] = t.a;
				colors[i * 24 + 4] = t.r;
				colors[i * 24 + 5] = t.g;
				colors[i * 24 + 6] = t.b;
				colors[i * 24 + 7] = t.a;
				colors[i * 24 + 8] = t.r;
				colors[i * 24 + 9] = t.g;
				colors[i * 24 + 10] = t.b;
				colors[i * 24 + 11] = t.a;
				colors[i * 24 + 12] = t.r;
				colors[i * 24 + 13] = t.g;
				colors[i * 24 + 14] = t.b;
				colors[i * 24 + 15] = t.a;
				colors[i * 24 + 16] = t.r;
				colors[i * 24 + 17] = t.g;
				colors[i * 24 + 18] = t.b;
				colors[i * 24 + 19] = t.a;
				colors[i * 24 + 20] = t.r;
				colors[i * 24 + 21] = t.g;
				colors[i * 24 + 22] = t.b;
				colors[i * 24 + 23] = t.a;

				params[i * 18] = t.x;
				params[i * 18 + 1] = t.y;
				params[i * 18 + 2] = t.rad;
				params[i * 18 + 3] = t.x;
				params[i * 18 + 4] = t.y;
				params[i * 18 + 5] = t.rad;
				params[i * 18 + 6] = t.x;
				params[i * 18 + 7] = t.y;
				params[i * 18 + 8] = t.rad;
				params[i * 18 + 9] = t.x;
				params[i * 18 + 10] = t.y;
				params[i * 18 + 11] = t.rad;
				params[i * 18 + 12] = t.x;
				params[i * 18 + 13] = t.y;
				params[i * 18 + 14] = t.rad;
				params[i * 18 + 15] = t.x;
				params[i * 18 + 16] = t.y;
				params[i * 18 + 17] = t.rad;
				params[i * 18 + 18] = t.x;
				params[i * 18 + 19] = t.y;
				params[i * 18 + 20] = t.rad;
				params[i * 18 + 21] = t.x;
				params[i * 18 + 22] = t.y;
				params[i * 18 + 23] = t.rad;
			}
			return this.vaoInfo;
		}

	}

	global.CircleEntity = global.CircleEntity || CircleEntity;

}(window));
