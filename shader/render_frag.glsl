#version 300 es
precision highp float;

uniform sampler2D posImage;
uniform sampler2D negImage;
uniform sampler2D original;
in vec2 v_uv;
out vec4 fragColor;

void main() {
  vec4 diff = clamp(texture(posImage, vec2(v_uv.s, v_uv.t)) - texture(negImage, vec2(v_uv.s, v_uv.t)), 0.0, 1.0);
  fragColor = vec4(diff.xyz,1.0);
}
