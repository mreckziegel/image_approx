#version 300 es
precision highp float;

in highp vec4 v_color;
in highp vec3 v_param;
in highp vec2 v_position;
in highp vec2 v_scale;
out vec4 fragData[2];

void main() {
  vec2 dist = (v_position - v_param.xy) * v_scale;
  vec4 color = (length(dist) <= v_param.z) ? v_color : vec4(0.0, 0.0, 0.0, 0.0);
  fragData[0] = clamp(color, 0.0, 1.0);
  fragData[1] = clamp(-1.0 * color, 0.0, 1.0);
}
