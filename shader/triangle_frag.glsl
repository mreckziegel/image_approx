#version 300 es
precision highp float;

in highp vec4 v_color;
out vec4 fragData[2];

void main() {
  fragData[0] = v_color;
  fragData[1] = -1.0 * v_color;
}
