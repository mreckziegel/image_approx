#version 300 es
in highp vec2 a_position;
in highp vec4 a_color;
in highp vec3 a_param;
uniform highp float sW;
uniform highp float sH;

out highp vec4 v_color;
out highp vec3 v_param;
out highp vec2 v_position;
out highp vec2 v_scale;

void main() {
  gl_Position = vec4(a_position,1.0, 1.0);
  v_color = a_color;
  v_param = a_param;
  v_position = a_position.xy;
  v_scale = vec2(sW, sH);
}
