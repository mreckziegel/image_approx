#version 300 es
precision highp float;

uniform sampler2D posImage;
uniform sampler2D negImage;
uniform sampler2D original;
in vec2 v_uv;
out vec4 fragData[1];

void main() {
  vec4 diff = texture(original, vec2(v_uv.s, v_uv.t)) - clamp(texture(posImage, vec2(v_uv.s, v_uv.t)) - texture(negImage, vec2(v_uv.s, v_uv.t)), 0.0, 1.0);
  fragData[0].r = dot( diff.xyz, diff.xyz);
  fragData[0].a = 1.0;
}
