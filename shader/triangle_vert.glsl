#version 300 es
in vec4 a_position;
in highp vec4 a_color;

out highp vec4 v_color;

void main() {
  gl_Position = a_position;
  v_color = a_color;
}
