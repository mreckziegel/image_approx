'use strict';

(function(global) {

	class Entity {
		constructor(origCanvas, renderCanvas, vs, fs) {
			renderCanvas = renderCanvas || document.createElement('canvas');
			this.width = origCanvas.width;
			this.height = origCanvas.height;
			this.origCanvas = origCanvas;
			this.renderer = new Renderer(origCanvas, renderCanvas, vs, fs);
		}
		/*
		// uniform crossover
		crossover(mother, father) {
			var son = [];
			var daughter = [];
			for(var i=0;i<mother.length;i++) {
				if(Math.random()<0.5) {
					son.push(mother[i]);
					daughter.push(father[i]);
				} else {
					daughter.push(mother[i]);
					son.push(father[i]);
				}
			}
			return [son,daughter];
		}
		*/
		// averaging crossover
		crossover(mother, father) {
			var child = [];
			for(var i=0;i<mother.length;i++) {
				var mT = mother[i];
				var fT = father[i];
				var cT = {};
				for(var j in mT) {
					cT[j] = (mT[j] + fT[j]) / 2;
				}
				child.push(cT);
			}
			return [child];
		}

		render(e) {
			this.renderer.render(this.getUpdatedVAOInfo(e));
		}

		getUniforms() {
			return null;
		}

		fitnessCPU(e) {
			var iL = this.width * this.height * 4;
			this.renderer.render(this.getUpdatedVAOInfo(e));
			var ctxOrig = this.origCanvas.getContext('2d');
			var gl = this.renderer.gl;
	    var p1 = ctxOrig.getImageData(0, 0, this.width, this.height).data;
			var p2 = new Uint8Array(iL);
	    gl.readPixels(0,0, this.width, this.height, gl.RGBA, gl.UNSIGNED_BYTE, p2);

	    var fitness = 0;
	    var i = 0;
	    while(i<iL) {
	      var dr = p1[i] - p2[i++];
	      var dg = p1[i] - p2[i++];
	      var db = p1[i] - p2[i++];
	      var pf = dr * dr + dg * dg + db * db;
	      fitness += pf; //add the pixel fitness to the total fitness ( lower is better )
	      i++; // skip alpha
	    }

	    return fitness;
		}

		fitness(e) {
			var fitness = this.renderer.fitness(this.getUpdatedVAOInfo(e));
			return fitness;
		}

		batchFitness(e, i) {
			this.renderer.batchFitness(this.getUpdatedVAOInfo(e), i);
		}

	}

	global.Entity = global.Entity || Entity;

}(window));
