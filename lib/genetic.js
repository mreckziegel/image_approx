// based on Genetic.js: https://github.com/subprotocol/genetic-js

var Genetic = Genetic || (function(){

	'use strict';

  var Optimize = {
    Maximize: function (a, b) { return a >= b; },
    Minimize: function (a, b) { return a < b; }
  };

	var Select1 = {
		Tournament2: function(pop) {
			var n = pop.length;
			var idxA = Math.floor(Math.random()*n);
			var idxB = Math.floor(Math.random()*n);
			var a = pop[idxA];
			var b = pop[idxB];
			return this.optimize(a.fitness, b.fitness) ? idxA : idxB;
		},
		Random: function (pop) {
			return Math.floor(Math.random()*pop.length);
		},
    Fittest: function (pop) {
			return 0;
		},
    RandomLinearRank: function (pop) {
			this.internalGenState["rlr"] = this.internalGenState["rlr"]||0;
      var idx = Math.floor(Math.random()*Math.min(pop.length,(this.internalGenState["rlr"]++)));
			return idx;
		},
    Sequential: function (pop) {
			this.internalGenState["seq"] = this.internalGenState["seq"]||0;
			return (this.internalGenState["seq"]++)%pop.length;
		}
	};

	var Select2 = {
		Tournament2: function(pop) {
			return [Select1.Tournament2.call(this, pop), Select1.Tournament2.call(this, pop)];
		},
		Tournament3: function(pop) {
			return [Select1.Tournament3.call(this, pop), Select1.Tournament3.call(this, pop)];
		},
		Random: function (pop) {
			return [Select1.Random.call(this, pop), Select1.Random.call(this, pop)];
		},
		RandomLinearRank: function (pop) {
			return [Select1.RandomLinearRank.call(this, pop), Select1.RandomLinearRank.call(this, pop)];
		},
		Sequential: function (pop) {
			return [Select1.Sequential.call(this, pop), Select1.Sequential.call(this, pop)];
		},
		FittestRandom: function (pop) {
			return [Select1.Fittest.call(this, pop), Select1.Random.call(this, pop)];
		}
	};

	function Genetic() {

		// population
		this.fitness = null;
		this.seed = null;
		this.mutate = null;
		this.crossover = null;
		this.select1 = null;
		this.select2 = null;
		this.optimize = null;
		this.notification = null;
		this.isPaused = false;

		this.configuration = {
			size: 250,
			crossover: 0.9,
			mutation: 0.2,
			iterations: 100,
			fittestAlwaysSurvives: true,
			skip: 0,
			rFactory: null
		};

		this.userData = {};
		this.internalGenState = {};

		this.pop = [];

    this.curIteration = 0;

    this.mutateOrNot =  function() {
      var doMutate = Math.random() <= this.configuration.mutation;
      return doMutate;
    }

    this.iterate = function() {
      var self = this;
      for (var i=0;i<=this.configuration.skip;++i) {
				// reset for each generation
				this.internalGenState = {};
				var newPop = [];

				if (this.configuration.fittestAlwaysSurvives) // lets the best solution fall through
					newPop.push({ entity:this.pop[0].entity, lIdx0: 0, mutated: false, fitness: this.pop[0].fitness });

				while (newPop.length < this.configuration.size) {
					if (
						Math.random() <= this.configuration.crossover // base crossover on specified probability
						&& newPop.length+1 < this.configuration.size // keeps us from going 1 over the max population size
					) {
						var pIdx = this.select2(this.pop);
						var children = this.configuration.rFactory.crossover(this.pop[pIdx[0]].entity, this.pop[pIdx[1]].entity);
            for(var j in children) {
              var mutate = this.mutateOrNot();
							var entity = mutate ? self.configuration.rFactory.mutate(children[j], self.userData) : children[j];
							var fitness = self.configuration.rFactory.fitness(entity);
  						newPop.push(
                  {
                    entity,
										fitness,
										mutated: mutate,
                    lIdx0: pIdx[0],
                    lIdx1: pIdx[1]
                  }
              );
							self.curIteration++;
            }
					} else {
            var idx = this.select1(this.pop);
            var mutate = this.mutateOrNot();
						var entity = mutate ? this.configuration.rFactory.mutate(this.pop[idx].entity, this.userData) : this.pop[idx].entity;
						var fitness = self.configuration.rFactory.fitness(entity);
						newPop.push(
              {
                entity,
								fitness,
								mutated:mutate,
                lIdx0: idx
              });
						self.curIteration++;
					}

				}
				this.pop = newPop.sort(function (a, b) {
						return self.optimize(a.fitness, b.fitness) ? -1 : 1;
				});
			}
      var isFinished = this.curIteration >= this.configuration.iterations;

      if (this.notification) {
        this.notification(this.pop, this.curIteration);
      }

      if(!isFinished && !this.isPaused) {
				window.requestAnimationFrame(this.iterate.bind(this));
      }
    }

		this.start = function() {

			var i;
			var self = this;
			var newPop = [];
			// seed the population
			for (i=0;i<this.configuration.size;++i)  {
  			newPop.push(this.configuration.rFactory.seed(this.userData));
			}
      // score and sort
      this.pop = newPop
        .map(function (entity) {
          var res = self.configuration.rFactory.fitness(entity);
          return {
            "fitness": res,
            "entity": entity,
            "lastIndex": -1
          };
        })
        .sort(function (a, b) {
          return self.optimize(a.fitness, b.fitness) ? -1 : 1;
        });

      this.iterate();
		}

	}

	Genetic.prototype.evolve = function(config, userData) {
		var k;
		for (k in config) {
			this.configuration[k] = config[k];
		}

		for (k in userData) {
			this.userData[k] = userData[k];
		}
    this.start();
	}

	return {
		create: function() {
			return new Genetic();
		},
		Select1, Select2, Optimize
	};

})();
