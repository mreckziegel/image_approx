'use strict';

function random(min, max) {
  return Math.random()*(max-min)+min;
}

// Standard Normal variate using Box-Muller transform.
function randn_bm() {
    var u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}

function clamp(v,min,max) {
  return Math.min(Math.max(v,min),max);
}

function mutateND(old, stddev, min, max) {
  return clamp(old + (randn_bm() * stddev),min,max);
}

function loadSync(filePath) {
    var req = new XMLHttpRequest();
    req.open("GET", filePath, false); // 'false': synchronous.
    req.send(null);
    return req.responseText;
}
