'use strict';

(function(global) {

  const fvs = loadSync('./shader/fitness_vert.glsl');
  const ffs = loadSync('./shader/fitness_frag.glsl');
  const rfs = loadSync('./shader/render_frag.glsl');


	class Renderer {
		constructor(origCanvas, renderCanvas, vs, fs, numElements) {
      const gl = this.gl = renderCanvas.getContext("webgl2");

      gl.getExtension('EXT_color_buffer_float');
      gl.getExtension('OES_texture_float');
      gl.getExtension('OES_texture_float_linear');

      this.origWidth = origCanvas.width;
      this.origHeight = origCanvas.height;
      this.renderWidth = renderCanvas.width;
      this.renderHeight = renderCanvas.height;

      const nextPow2 = this.nextPow2 = Math.pow(2, Math.ceil(Math.log2(Math.max(renderCanvas.width, renderCanvas.height)/Math.log2(2))));
      const mipLevel = this.mipLevel = Math.log2(this.nextPow2);
      const textures = this.textures = twgl.createTextures(gl, {
        pos: {
          width: renderCanvas.width,
          height: renderCanvas.height,
          auto: false,
          minMag: gl.LINEAR,
          wrap: gl.CLAMP_TO_EDGE,
        },
        neg: {
          width: renderCanvas.width,
          height: renderCanvas.height,
          auto: false,
          minMag: gl.LINEAR,
          wrap: gl.CLAMP_TO_EDGE,
        },
        original: {
          width: origCanvas.width,
          height: origCanvas.height,
          auto: false,
          minMag: gl.LINEAR,
          wrap: gl.CLAMP_TO_EDGE,
          src: origCanvas
        },
        fit: {
          width: nextPow2,
          height: nextPow2,
          auto: false,
          minMag: gl.LINEAR,
          wrap: gl.CLAMP_TO_EDGE,
          format: gl.RED,
          internalFormat: gl.R32F,
          type: gl.FLOAT
        }
      });

      this.fitnessUniforms = {
        posImage: textures.pos,
        negImage: textures.neg,
        original: textures.original
      }

      // save aspect ratios
      this.entityUniforms = {
        sW: Math.max(1, renderCanvas.width / renderCanvas.height),
        sH: Math.max(1, renderCanvas.height / renderCanvas.width)
      }

      this.entityProgram = twgl.createProgramInfo(gl, [vs, fs]);
      this.renderProgram = twgl.createProgramInfo(gl, [fvs, rfs]);
      this.fitnessProgram = twgl.createProgramInfo(gl, [fvs, ffs]);
      this.fitnessData = new Float32Array(4);

      this.fitnessQuadVaoInfo = this.createVAOInfo(this.fitnessProgram, {
         a_position: { data: new Float32Array([-1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0]), numComponents: 2, usage: gl.STATIC_DRAW},
         a_uv: { data: new Float32Array([0, 1.0, 1.0, 1.0, 0, 0, 1.0, 0]), numComponents: 2, usage: gl.STATIC_DRAW}
      }, 4);
      this.updateBuffers(this.fitnessQuadVaoInfo);

      const attachments = [
        { attach: gl.COLOR_ATTACHMENT0, attachment: this.textures.pos},
        { attach: gl.COLOR_ATTACHMENT1, attachment: this.textures.neg}
      ];

      this.efb = twgl.createFramebufferInfo(gl, attachments);
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.efb.framebuffer);
      gl.drawBuffers([gl.COLOR_ATTACHMENT0, gl.COLOR_ATTACHMENT1]);


      const fAttachment = [
        { attach: gl.COLOR_ATTACHMENT0, attachment: this.textures.fit}
      ];

      this.ffb = twgl.createFramebufferInfo(gl, fAttachment);
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.ffb.framebuffer);
      gl.drawBuffers([gl.COLOR_ATTACHMENT0]);

      gl.enable(gl.BLEND);
      gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
      //gl.blendFunc(gl.ONE, gl.ONE);
      gl.disable(gl.DEPTH_TEST);
      gl.clearColor(0.0, 0.0, 0.0, 1.0);
      this.fbr = gl.createFramebuffer();
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbr);
      gl.drawBuffers([gl.COLOR_ATTACHMENT0]);
      gl.readBuffer(gl.COLOR_ATTACHMENT0);
      gl.bindBuffer(gl.PIXEL_PACK_BUFFER, null);
		}

    createVAOInfo(programInfo, att, count) {
      var res = {};
      var gl = this.gl;
      var vao = gl.createVertexArray();
      gl.bindVertexArray(vao);
      var res = { vao, buffers: {}, count }

      for(var name in att) {
        var item = att[name];
        var attLoc = gl.getAttribLocation(programInfo.program, name);
        var glBuf = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, glBuf);
        gl.bufferData(gl.ARRAY_BUFFER, item.data.byteLength, item.usage || gl.DYNAMIC_DRAW);
        gl.enableVertexAttribArray(attLoc);
        gl.vertexAttribPointer(attLoc, item.numComponents, gl.FLOAT, false, 0, 0);
        res.buffers[name] = {
          glBuf, data: item.data
        };
      }
      return res;
    }

    updateBuffers(vaoInfo) {
      const gl = this.gl;
      for(var i in vaoInfo.buffers) {
        var buffer = vaoInfo.buffers[i];
        gl.bindBuffer(gl.ARRAY_BUFFER, buffer.glBuf);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, buffer.data);
      }
    }

    render(vaoInfo) {
      const gl = this.gl;
      const quadVaoInfo = this.fitnessQuadVaoInfo;
      gl.bindVertexArray(vaoInfo.vao);
      this.updateBuffers(vaoInfo);
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.efb.framebuffer);
      gl.useProgram(this.entityProgram.program);
      twgl.setUniforms(this.entityProgram, this.entityUniforms);
      gl.viewport(0, 0, this.renderWidth, this.renderHeight);
      gl.clear(gl.COLOR_BUFFER_BIT);
      gl.drawArrays(gl.TRIANGLES, 0, vaoInfo.count);
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      gl.useProgram(this.renderProgram.program);
      twgl.setUniforms(this.renderProgram, this.fitnessUniforms);
      gl.bindVertexArray(quadVaoInfo.vao);
      gl.clear(gl.COLOR_BUFFER_BIT);
      gl.drawArrays(gl.TRIANGLE_STRIP, 0, quadVaoInfo.count);
    }

    fitness(vaoInfo) {
      const gl = this.gl;
      const quadVaoInfo = this.fitnessQuadVaoInfo;
      this.updateBuffers(vaoInfo);
      gl.bindVertexArray(vaoInfo.vao);
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.efb.framebuffer);
      gl.useProgram(this.entityProgram.program);
      twgl.setUniforms(this.entityProgram, this.entityUniforms)
      gl.viewport(0, 0, this.renderWidth, this.renderHeight);
      gl.clear(gl.COLOR_BUFFER_BIT);
      gl.drawArrays(gl.TRIANGLES, 0, vaoInfo.count);

      gl.bindVertexArray(quadVaoInfo.vao);
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.ffb.framebuffer);
      gl.useProgram(this.fitnessProgram.program);
      twgl.setUniforms(this.fitnessProgram, this.fitnessUniforms);
      gl.viewport(0, 0, this.nextPow2, this.nextPow2);
      gl.clear(gl.COLOR_BUFFER_BIT);
      gl.drawArrays(gl.TRIANGLE_STRIP, 0, quadVaoInfo.count);

      gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbr);
      gl.bindTexture(gl.TEXTURE_2D, this.textures.fit);
      gl.generateMipmap(gl.TEXTURE_2D);
      gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.textures.fit, this.mipLevel);
      gl.readPixels(0, 0, 1, 1, gl.RGBA, gl.FLOAT, this.fitnessData);
      return this.fitnessData[0];
    }
	}

	global.Renderer = global.Renderer || Renderer;

}(window));
