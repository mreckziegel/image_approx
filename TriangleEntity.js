'use strict';

(function(global) {

	const vs = loadSync('./shader/triangle_vert.glsl');
	const fs = loadSync('./shader/triangle_frag.glsl');

	class TriangleEntity extends Entity {
		constructor(origCanvas, renderCanvas, length) {
			super(origCanvas, renderCanvas, vs, fs);

			this.vaoInfo = this.renderer.createVAOInfo(this.renderer.entityProgram, {
				 a_position: { data: new Float32Array(length * 3 * 2), numComponents: 2},
				 a_color: { data: new Float32Array(length * 3 * 4), numComponents: 4}
			}, length * 3);
		}

		seed(userData) {
			var e = [];

			for(var i=0;i<userData.l;i++) {
				var t = {};
				var x = random(-1, 1);
				var y = random(-1, 1);
				t.x1 = t.x2 = t.x3 = x;
				t.y1 = t.y2 = t.y3 = y;
				t.r = random(0, 1);
				t.g = random(0, 1);
				t.b = random(0, 1);
				t.a = 0;//random(0, 0.5);
				e.push(t);
			}
			return e;
		}

		mutate(e, userData) {
			var res = [];
			var temp = userData.pt;
			var cTemp = userData.ct;
			var numMutate = userData.nm;

			for(var i=0;i<e.length;i++) {
				var t = e[i];
				var newT = { x1 : t.x1, x2: t.x2, x3: t.x3, y1: t.y1, y2: t.y2, y3: t.y3, r: t.r, g: t.g, b: t.b, a: t.a};
				res.push(newT);
			}

			var idx = [];
			for(var i=0;i<numMutate;i++) {
				idx.push(Math.round(random(0,e.length - 1)));
			}

			for(var i=0;i<numMutate;i++) {
				var oldT = e[idx[i]];
				var newT = {};
				newT.x1 = mutateND(oldT.x1, temp, -1, 1);
				newT.x2 = mutateND(oldT.x2, temp, -1, 1);
				newT.x3 = mutateND(oldT.x3, temp, -1, 1);

				newT.y1 = mutateND(oldT.y1, temp, -1, 1);
				newT.y2 = mutateND(oldT.y2, temp, -1, 1);
				newT.y3 = mutateND(oldT.y3, temp, -1, 1);

				newT.r = mutateND(oldT.r, cTemp, 0, 1);
				newT.g = mutateND(oldT.g, cTemp, 0, 1);
				newT.b = mutateND(oldT.b, cTemp, 0, 1);
				newT.a = mutateND(oldT.a, cTemp, 0, 0.5);
				res[idx[i]] = newT;
			}
			return res;
		}


		getUpdatedVAOInfo(e) {
			var positions = this.vaoInfo.buffers.a_position.data;
			var colors = this.vaoInfo.buffers.a_color.data;
			for(var i=0;i<e.length;i++) {
				var t = e[i];
				positions[i * 6] = t.x1;
				positions[i * 6 + 1] = t.y1;
				positions[i * 6 + 2] = t.x2;
				positions[i * 6 + 3] = t.y2;
				positions[i * 6 + 4] = t.x3;
				positions[i * 6 + 5] = t.y3;
				colors[i * 12] = t.r;
				colors[i * 12 + 1] = t.g;
				colors[i * 12 + 2] = t.b;
				colors[i * 12 + 3] = t.a;
				colors[i * 12 + 4] = t.r;
				colors[i * 12 + 5] = t.g;
				colors[i * 12 + 6] = t.b;
				colors[i * 12 + 7] = t.a;
				colors[i * 12 + 8] = t.r;
				colors[i * 12 + 9] = t.g;
				colors[i * 12 + 10] = t.b;
				colors[i * 12 + 11] = t.a;
			}
			return this.vaoInfo;
		}

	}

	global.TriangleEntity = global.TriangleEntity || TriangleEntity;

}(window));
